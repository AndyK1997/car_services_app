
package com.example.car_app;

        import android.content.Intent;
        import android.database.sqlite.SQLiteDatabase;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.ProgressBar;
        import android.widget.Toast;

        import com.example.car_app.ui.dashboard.CardItem;
        import com.google.android.material.bottomnavigation.BottomNavigationView;

        import androidx.appcompat.app.AppCompatActivity;
        import androidx.navigation.NavController;
        import androidx.navigation.Navigation;
        import androidx.navigation.ui.AppBarConfiguration;
        import androidx.navigation.ui.NavigationUI;

        import java.util.ArrayList;

public class UserInterface extends AppCompatActivity {
    private DataBaseHelper dataBaseHelper;
    private SQLiteDatabase mDatabase;
    private ArrayList<CardItem> mDataServiceList;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userinterface);
        //this.deleteDatabase("carServiceAppDataBase");
        dataBaseHelper=new DataBaseHelper(this);
        mDatabase = getDataBaseHelper().getWritableDatabase();
        progressBar=findViewById(R.id.progressRing);
        progressBar.setVisibility(View.INVISIBLE);
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("SE");
        mDataServiceList=bundle.getParcelableArrayList("S");
        boolean TEST =false;
        try {
            //showProgressRing(true);
            //dataBaseHelper.createUser(mDatabase,"Andreas","A25.7.hiG$","Corolla E11","Toyota","OF TK 620");
            //dataBaseHelper.createDealer(mDatabase,"Autohaus Stumpf","Am Glockenturm",4,63814,"Mainaschaff","+49 6021 79080","kontakt@toyota-stumpf.de");
        }catch (Exception e){
            createThreadedToast(e.getMessage());
        }

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications, R.id.settingsFragment).build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);


    }

    /**
     * create a Thread to show a Toast
     * @param MessageText
     */
    public void createThreadedToast(final String MessageText){
        final Toast toast=Toast.makeText(this,MessageText,Toast.LENGTH_SHORT);

        try {
            Runnable newTread = new Runnable() {
                @Override
                public void run() {
                    synchronized (this) {
                        toast.show();
                    }
                }
            };
            Thread thread = new Thread(newTread);
            thread.start();
        }catch (Exception e){

        }
    }


    /**
     * show or hide Indeterminate progress ring
     * @param show
     */
    public void showProgressRing(boolean show){
        if(show==true){
            this.progressBar.setVisibility(View.VISIBLE);
        }else{
            this.progressBar.setVisibility(View.INVISIBLE);
        }
    }

    public ArrayList<CardItem> getmDataServiceList() {
        return mDataServiceList;
    }

    public SQLiteDatabase getmDatabase(){
        return  this.mDatabase;
    }

    public DataBaseHelper getDataBaseHelper() {
        return dataBaseHelper;
    }
}

package com.example.car_app;

public class CurrentUser {
    private int id;
    private String userName;
    private String carModel;
    private String carBrand;
    private String licenseplate;

    public CurrentUser(int id, String userName, String carModel,String carBrand, String licenseplate){
        this.setId(id);
        this.setUserName(userName);
        this.setCarModel(carModel);
        this.setCarBrand(carBrand);
        this.setLicenseplate(licenseplate);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getLicenseplate() {
        return licenseplate;
    }

    public void setLicenseplate(String licenseplate) {
        this.licenseplate = licenseplate;
    }
}


package com.example.car_app.ui.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

public class CardItem  implements Parcelable {

    private int mImageResource;
    private String mLine1;
    private String mLine2;
    private String mDate;
    private int serviceId;
    public CardItem(int ImageResource, String Line1, String Line2, String Date, int serviceId){
        this.mImageResource=ImageResource;
        this.mLine1=Line1;
        this.mLine2=Line2;
        this.mDate=Date;
        this.serviceId=serviceId;
    }

    protected CardItem(Parcel in) {
        mImageResource = in.readInt();
        mLine1 = in.readString();
        mLine2 = in.readString();
        mDate = in.readString();
        serviceId=in.readInt();
    }


    public static final Creator<CardItem> CREATOR = new Creator<CardItem>() {
        @Override
        public CardItem createFromParcel(Parcel in) {
            return new CardItem(in);
        }

        @Override
        public CardItem[] newArray(int size) {
            return new CardItem[size];
        }
    };

    public int getmImageResource() {
        return mImageResource;
    }

    public String getmLine1() {
        return mLine1;
    }

    public String getmLine2() {
        return mLine2;
    }

    public String getmDate() {
        return mDate;
    }


    public int getServiceId() {
        return serviceId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mImageResource);
        dest.writeString(mLine1);
        dest.writeString(mLine2);
        dest.writeString(mDate);
        dest.writeInt(serviceId);
    }
}

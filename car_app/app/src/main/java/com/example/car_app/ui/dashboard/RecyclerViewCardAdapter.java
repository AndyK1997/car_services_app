package com.example.car_app.ui.dashboard;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.car_app.R;
import java.util.ArrayList;

public class RecyclerViewCardAdapter extends RecyclerView.Adapter<RecyclerViewCardAdapter.RecyclerViewCardAdapterViewHolder> {
    private ArrayList<CardItem> mDataset;
    private OnItemClickListener mListener;
    /**
     * On click interface
     */
    public interface OnItemClickListener{
        void oItemClick(int position);
    }

    /**
     * Set on click interface
     * @param listener
     */
    public void setOnItemClickListener(OnItemClickListener listener){
        mListener=listener;
    }

    /**
     * Innerclass of RecyclerviewAdapter to create a view Holder
     */
    public static class RecyclerViewCardAdapterViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mLine1;
        public TextView mLine2;
        public TextView mDate;

        /**
         * View holder Constructor
         * @param itemView
         */
        public RecyclerViewCardAdapterViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            this.mImageView= itemView.findViewById(R.id.imageView);
            this.mLine1 = itemView.findViewById(R.id.textViewLine1);
            this.mLine2=itemView.findViewById(R.id.textViewLine2);
            this.mDate=itemView.findViewById(R.id.textViewDate);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position=getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            listener.oItemClick(position);
                        }
                    }
                }
            });
        }
    }

    /**
     * RecyclerviewAdapter constructor
     * @param Dataset
     */
    public RecyclerViewCardAdapter(ArrayList<CardItem> Dataset){
        this.mDataset=Dataset;
    }

    /**
     * On Create status of the Adapter
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public RecyclerViewCardAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent,false);
        RecyclerViewCardAdapterViewHolder newRecyclerViewCardAdapterViewHolder =new  RecyclerViewCardAdapterViewHolder(v,mListener);
        return newRecyclerViewCardAdapterViewHolder;
    }

    /**
     * In this function will set the current  visable items on the screen
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull RecyclerViewCardAdapterViewHolder holder, int position) {
        CardItem currentItem=this.mDataset.get(position);
        holder.mImageView.setImageResource(currentItem.getmImageResource());
        holder.mLine1.setText(currentItem.getmLine1());
        holder.mLine2.setText(currentItem.getmLine2());
        holder.mDate.setText(currentItem.getmDate());
    }

    /**
     * return how many element dataset contains
     * @return
     */
    @Override
    public int getItemCount() {

        return this.mDataset.size();
    }
}

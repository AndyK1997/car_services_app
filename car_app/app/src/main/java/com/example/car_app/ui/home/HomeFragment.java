package com.example.car_app.ui.home;

import android.content.Intent;
import android.database.Cursor;
import android.icu.lang.UScript;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.car_app.MainActivity;
import com.example.car_app.R;
import com.example.car_app.UserInterface;

public class HomeFragment extends Fragment {
    private TextView userName;
    private TextView carModel;
    private TextView serviceCount;
    private EditText inputService;
    private Button searchInputServiceButton;
    private TextView searchedServiceDate;
    private Button showDetailOfServiceButton;
    private Cursor myCursor;
    private Intent detailsIntent;
    //private HomeViewModel homeViewModel;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        this.userName=(TextView)root.findViewById(R.id.showUserName);
        this.carModel=(TextView)root.findViewById(R.id.showCarModel);
        this.serviceCount=(TextView)root.findViewById(R.id.showCountServices);
        this.inputService=(EditText)root.findViewById(R.id.inputServiceSearche);
        this.searchInputServiceButton=(Button)root.findViewById(R.id.searchButton);
        this.searchedServiceDate=(TextView)root.findViewById(R.id.searchedServiceDate);
        this.showDetailOfServiceButton=(Button)root.findViewById(R.id.showServiceDetails);
        setServiceSearchButtonListener();
        setCountOfallServices();
        return root;
    }

    public void setServiceSearchButtonListener(){
        searchInputServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evaluateSearchResult();
            }
        });
    }

    public void setCountOfallServices(){
        int count=((UserInterface)getActivity()).getmDataServiceList().size();
        this.serviceCount.setText("Services:"+count);
    }


    /**
     * set the found  Service Date if Service exist and load the Intent for The Detail view
     */
    public void evaluateSearchResult(){
        myCursor=((UserInterface)getActivity()).getDataBaseHelper().searchLatestSearchedService(((UserInterface)getActivity()).getmDatabase(),inputService.getText().toString());
        if(myCursor.getCount()>=1){
            searchedServiceDate.setText(myCursor.getString(myCursor.getColumnIndex("DATE_BILL")));
            detailsIntent=new Intent(getContext(),UserInterface.class);
            Bundle b=new Bundle();
            b.putInt("Id_Service",myCursor.getInt(myCursor.getColumnIndex("ID_SERVICE")));
            detailsIntent.putExtra("Service",b);
        }else{
            searchedServiceDate.setText("N/A");
            ((UserInterface)getActivity()).createThreadedToast("es wurde kein Service gefunden");
        }
    }
}

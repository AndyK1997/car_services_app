package com.example.car_app.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.example.car_app.MainActivity;
import com.example.car_app.R;
import com.example.car_app.UserInterface;

public class SettingsFragment extends Fragment {
    private SeekBar seekBar;
    private RadioGroup radioGroup;
    private RadioButton  radioButton;
    private CheckBox checkBox;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_settings, container, false);
        createSeekbarforBrightnessControl(root);
        createRadioGroupWithListener(root);
        createCheckboxWithListener(root);
        return root;
    }

    /**
     * create the Checkbox to turn on/off the dark mode feature
     * @param root
     */
    public void createCheckboxWithListener(final View root){
        checkBox=root.findViewById(R.id.alowNotification);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkBox.isChecked()){
                    ((UserInterface)getActivity()).createThreadedToast("Benachrichtigungen wurden erlaubt");
                }else{
                    ((UserInterface)getActivity()).createThreadedToast("Benachrichtigungen gesperrt");
                }
            }
        });
    }

    /**
     * Create the Radiogroup for the turn on/off dark mode feature
     * @param root
     */
    public void createRadioGroupWithListener(final View root){
        radioGroup=root.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                //gibt den ausgewählten button zurück
                radioButton=root.findViewById(checkedId);
                ((UserInterface)getActivity()).createThreadedToast(radioButton.getText().toString());
            }
        });
    }

    /**
     * Create a Seekbar to control the device Brightness
     * @param view
     */
    public void createSeekbarforBrightnessControl(View view){
        seekBar=(SeekBar)view.findViewById(R.id.seek_bar);

        int currentBrightness= Settings.System.getInt(getContext().getContentResolver(),Settings.System.SCREEN_BRIGHTNESS,0);
        seekBar.setProgress(currentBrightness);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Context context=getContext();
                boolean canWrite=Settings.System.canWrite(context);

                if(canWrite){
                    int setBrightness=progress*255/255;
                    Settings.System.putInt(context.getContentResolver(),Settings.System.SCREEN_BRIGHTNESS_MODE,Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
                    Settings.System.putInt(context.getContentResolver(),Settings.System.SCREEN_BRIGHTNESS,setBrightness);
                }else {
                    Intent intent =new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                    context.startActivity(intent);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
}


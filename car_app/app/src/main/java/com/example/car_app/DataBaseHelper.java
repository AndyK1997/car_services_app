package com.example.car_app;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.service.autofill.LuhnChecksumValidator;
import android.widget.Toast;

import java.security.*;

import androidx.annotation.Nullable;

import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Date;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class DataBaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME ="carServiceAppDataBase";
    public static final int DATABASE_VERSION=1;
    public DataBaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_SCRIPT_TABLE_USER="CREATE TABLE USER(ID_USER INTEGER PRIMARY KEY AUTOINCREMENT, USER_NAME TEXT NOT NULL,"+
                " USER_PASSWORD BLOB NOT NULL, CARMODEL TEXT ,CARBRAND TEXT ,LICENSEPLATE TEXT NOT NULL, PASS_SALT BLOB NOT NULL)";

        final String SQL_CREATE_SCRIPT_TABLE_DEALERSHIP="CREATE TABLE DEALERSHIP(ID_DEALERSHIP INTEGER PRIMARY KEY AUTOINCREMENT,"+
                " DEALER_NAME TEXT NOT NULL, STREET TEXT, HOUSENUMBER TEXT, POSTCODE INTEGER,CITY TEXT,PHONE TEXT NOT NULL,EMAIL TEXT NOT NULL)";

        final String SQL_CREATE_SCRIPT_TABLE_SERVICE="CREATE TABLE SERVICE(ID_SERVICE INTEGER PRIMARY KEY AUTOINCREMENT, SERVICE_TYPE TEXT NOT NULL,"+
                " DESCRIPTION TEXT, PRICE REAL NOT NULL, DATE_BILL TEXT NOT NULL, ID_DEALERSHIP INTEGER, ID_USER INTEGER,"+
                " CONSTRAINT fk_DEALERSHIP FOREIGN KEY (ID_DEALERSHIP) REFERENCES DEALERSHIP(ID_DEALERSHIP),"+
                " CONSTRAINT fk_USER FOREIGN KEY (ID_USER) REFERENCES USER(ID_USER))";

        db.execSQL(SQL_CREATE_SCRIPT_TABLE_USER);
        db.execSQL(SQL_CREATE_SCRIPT_TABLE_DEALERSHIP);
        db.execSQL(SQL_CREATE_SCRIPT_TABLE_SERVICE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS USER");
        db.execSQL("DROP TABLE IF EXISTS DEALERSHIP");
        db.execSQL("DROP TABLE IF EXISTS SERVICE");
        onCreate(db);
    }


    /**
     * validate input password compare database password
     * @param mDatabase
     * @param UserName
     * @param InputPassword
     * @return
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     */
    public boolean validatePassword(SQLiteDatabase mDatabase,String UserName, String InputPassword) throws Exception {
        boolean correctPassword=false;

        String[] columbs=new String[]{"USER_PASSWORD","PASS_SALT"};
        String[] args =new String[]{UserName};
       Cursor myCursor = searchSqlUserElement(mDatabase,columbs,args);
        if(myCursor.getCount()>=1){
            //USER existiert
            byte[] salt=myCursor.getBlob(myCursor.getColumnIndex( "PASS_SALT"));
            byte[] hash=createHash(InputPassword,salt);
            byte[] pw=myCursor.getBlob(myCursor.getColumnIndex("USER_PASSWORD"));
            if(Arrays.equals(hash,pw)){
                correctPassword=true;
            }else{
                //TOAST falsches Password;
                correctPassword=false;
            }
        }else{
            //USER existiert nicht
            throw new Exception("User existiert nicht");
        }
        return correctPassword;
    }

    /**
     * serch a define user in the database on Table User
     * @param mDatabase
     * @param columns
     * @param UserName
     * @return
     */
    public Cursor searchSqlUserElement(SQLiteDatabase mDatabase, String[] columns, String[] UserName){

       // final String Table="USER";
        final String selection="USER_NAME=?";
        Cursor myCursor = mDatabase.query("USER",columns,selection,UserName,null,null,null);
        if(myCursor.getCount()>=1) {
            myCursor.moveToFirst();
        }

         return myCursor;
    }

    /**
     * Hashing algorithm for passwort
     * @param password
     * @param Salt
     * @return PasswordHash
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public byte[] createHash(String password, byte[] Salt)throws NoSuchAlgorithmException, InvalidKeySpecException{
        byte[] hash=null;
        KeySpec spec = new PBEKeySpec(password.toCharArray(),Salt,65536,128);
        SecretKeyFactory factory =SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        hash=factory.generateSecret(spec).getEncoded();
        return hash;
    }

    /**
     * create a Random salt for passwort hash
     * @return salt
     */
    public byte[] createSalt(){
        byte[] Salt;
            SecureRandom random =new SecureRandom();
            Salt=new byte[16];
            random.nextBytes(Salt);
        return Salt;
    }

    /**
     * Write a user in the Sqlite Database User Table
     * @param mDatabase
     * @param UserName
     * @param Password
     * @param CarModel
     * @param CarBrand
     * @param Licenseplate
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     */
    public void createUser(SQLiteDatabase mDatabase, String UserName, String Password,String CarModel, String CarBrand,String Licenseplate) throws InvalidKeySpecException, NoSuchAlgorithmException {
        ContentValues UserCv=new ContentValues();
        byte[] Salt=createSalt();
        UserCv.put("USER_NAME",UserName);
        UserCv.put("USER_PASSWORD",createHash(Password,Salt));
        UserCv.put("CARMODEL",CarModel);
        UserCv.put("CARBRAND",CarBrand);
        UserCv.put("LICENSEPLATE", Licenseplate);
        UserCv.put("PASS_SALT",Salt);
        mDatabase.insert("USER",null,UserCv);
    }

    /**
     * Writr a Dealer in the Sqlite Database Dealership Table
     * @param mDatabase
     * @param name
     * @param street
     * @param houseNumber
     * @param postcode
     * @param city
     * @param phone
     * @param email
     */
    public void createDealer(SQLiteDatabase mDatabase,String name, String street,String houseNumber,int postcode,String city, String phone,String email){
        ContentValues dealerCv =new ContentValues();
        dealerCv.put("DEALER_NAME",name);
        dealerCv.put("STREET",street);
        dealerCv.put("HOUSENUMBER",houseNumber);
        dealerCv.put("POSTCODE",postcode);
        dealerCv.put("CITY",city);
        dealerCv.put("PHONE",phone);
        dealerCv.put("EMAIL",email);
        mDatabase.insert("DEALERSHIP",null,dealerCv);
    }

    /**
     * Write a Service in the Sqlite Database Service Table
     * @param mDatabase
     * @param userID
     * @param dealerID
     * @param serviceTyp
     * @param description
     * @param price
     * @param billDate
     */
    public void createService(SQLiteDatabase mDatabase, int userID, int dealerID, String serviceTyp, String description, float price, Date billDate){
        ContentValues ServiceCv=new ContentValues();
        ServiceCv.put("SERVICE_TYPE",serviceTyp);
        ServiceCv.put("DESCRIPTION",description);
        ServiceCv.put("PRICE",price);
        ServiceCv.put("DATE_BILL",billDate.toString());
        ServiceCv.put("ID_DEALERSHIP",dealerID);
        ServiceCv.put("ID_USER",userID);
        mDatabase.insert("SERVICE",null,ServiceCv);

    }

    /**
     * search a define Dealer in te Database on Table Dealership
     * @param mDatabase
     * @param dealerName
     * @return
     */
    public Cursor searchSqlDealerElement(SQLiteDatabase mDatabase, String dealerName){
        Cursor myCursor;
        String[] columbs=new String[]{"ID_DEALERSHIP","DEALER_NAME","STREET","HOUSENUMBER","POSTCODE","CITY","PHONE","EMAIL"};
        String[] Name=new String[]{dealerName};
        final String selection="DEALER_NAME=?";
        myCursor=mDatabase.query("DEALERSHIP",columbs,selection,Name,null,null,null);
        if(myCursor.getCount()>=1) {
            myCursor.moveToFirst();
        }
        return myCursor;
    }


    public Cursor searchSqlDealerName(SQLiteDatabase mDatabase, int dealerID){
        Cursor myCursor;
        String[] columbs=new String[]{"DEALER_NAME"};
        String[] Name=new String[]{Integer.toString(dealerID)};
        final String selection="ID_DEALERSHIP=?";
        myCursor=mDatabase.query("DEALERSHIP",columbs,selection,Name,null,null,null);
        if(myCursor.getCount()>=1) {
            myCursor.moveToFirst();
        }
        return myCursor;
    }


    /**
     * search a define SERVICE IN THE   Database on TABLE Service
     * @param mDatabase
     * @param userID
     * @return
     */
    public Cursor searchSqlService(SQLiteDatabase mDatabase, int userID){
        Cursor myCursor;
        String[] columbs=new String[]{"SERVICE_TYPE","DESCRIPTION","PRICE","DATE_BILL","ID_DEALERSHIP","ID_USER","ID_SERVICE"};
        String[]  serviceid=new String[]{Integer.toString(userID)};
        final String selection="ID_USER=?";
        myCursor=mDatabase.query("SERVICE",columbs,selection,serviceid,null,null,null);
        if(myCursor.getCount()>=1) {
            myCursor.moveToFirst();
        }
        return myCursor;
    }

    /**
     * return all Elements of given Table DESC
     * @param mDatabase
     * @param TableName
     * @return
     */
    public Cursor searchAllElementsInDatabase(SQLiteDatabase mDatabase, String TableName){
        Cursor mCursor;
        mCursor= mDatabase.query(TableName,null,null,null,null,null,"ID_"+TableName+" DESC");
        if(mCursor.getCount()>=1){
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Return the first search Service hit
     * @param mDatabase
     * @param searchInput
     * @return
     */
    public Cursor searchLatestSearchedService(SQLiteDatabase mDatabase,String searchInput){
        Cursor mCursor;
        String[] columbs=new String[]{"ID_SERVICE","SERVICE_TYPE","DATE_BILL"};
        String [] args=new String[]{"%"+searchInput+"%"};
        final String selection="DESCRIPTION LIKE ?";

        mCursor=mDatabase.query("SERVICE",columbs,selection,args,null,null, "DATE_BILL DESC");
        if(mCursor.getCount()>=1){
            mCursor.moveToFirst();
            String test=mCursor.getString(mCursor.getColumnIndex("DATE_BILL"));
        }
        return mCursor;
    }

}
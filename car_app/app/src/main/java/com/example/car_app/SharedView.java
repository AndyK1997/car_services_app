package com.example.car_app;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.car_app.ui.dashboard.CardItem;

import java.util.ArrayList;

public class SharedView extends ViewModel {
    private MutableLiveData<ArrayList<CardItem>> sharedData =new MutableLiveData<>();

    public void setSharedData( ArrayList<CardItem> input){
        sharedData.setValue(input);
    }

    public LiveData<ArrayList<CardItem>> getSharedData(){
        return sharedData;
    }

    public ArrayList<CardItem> getCardList(){
        return sharedData.getValue();
    }

}

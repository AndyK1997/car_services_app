package com.example.car_app;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.car_app.ui.dashboard.CardItem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private CurrentUser user;
    private Button  loginButton;
    private Button changeUserButton;
    private TextView showCurrentUserTextView;
    private EditText passwordInputEditText;
    private SQLiteDatabase mDatabase;
    private DataBaseHelper mDbHelper;
    private ArrayList<CardItem> mDataServiceList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDbHelper=new DataBaseHelper(this);
        mDatabase =mDbHelper.getWritableDatabase();
        loginButton=findViewById(R.id.LoginButton);
        changeUserButton=findViewById(R.id.ChangeUser);
        showCurrentUserTextView=findViewById(R.id.lastLogedInUserName);
        passwordInputEditText=findViewById(R.id.InputPassword);

        try {
            //mDbHelper.createUser(mDatabase,"Andreas","A25.7.hiG$","Corolla E11","Toyota","OF TK 620");
            setCurrentUserTextView();
            setLoginOnClickListener();
        }catch (Exception e){
            Toast.makeText(this,e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * set the Text of the last logged in User
     */
    public void setCurrentUserTextView() throws Exception{
        loadDatafromSharedPreferences();
        showCurrentUserTextView.setText(user.getUserName());
    }

    /**
     * Set Login Button OnClick listener
     */
    public void setLoginOnClickListener(){
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    boolean passwordCorrect= mDbHelper.validatePassword(mDatabase,user.getUserName(),passwordInputEditText.getText().toString());
                    if(passwordCorrect==true){
                        mDataServiceList =loadDatafromDatabase();
                        createUserInterface();
                    }else {
                        Toast.makeText(getApplicationContext(),"falsches Passwort",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * load Card Data from the database
     * @return
     */
    public ArrayList<CardItem> loadDatafromDatabase() throws Exception{
        mDataServiceList=new ArrayList<>();

        //createTmpDealerAndServiceData();
        Cursor mCursor=mDbHelper.searchSqlService(mDatabase,user.getId());
        while (mCursor.moveToNext()){
            Cursor tCursor=mDbHelper.searchSqlDealerName(mDatabase,mCursor.getInt(mCursor.getColumnIndex("ID_DEALERSHIP")));
            String dealer=tCursor.getString(tCursor.getColumnIndex("DEALER_NAME"));
            String serviceTyp=mCursor.getString(mCursor.getColumnIndex("SERVICE_TYPE"));
            String billDate=mCursor.getString(mCursor.getColumnIndex("DATE_BILL"));
            int serviceID=mCursor.getInt(mCursor.getColumnIndex("ID_SERVICE"));
            this.mDataServiceList.add(new CardItem(R.drawable.ic_dashboard_black_24dp,serviceTyp,dealer,billDate,serviceID));
            mCursor.moveToNext();
        }
        return this.mDataServiceList;
    }

    /**
     * Create the User Interface Activity
     */
    public void createUserInterface(){
        mDatabase.close();
        Intent intent=new Intent(this,UserInterface.class);
        Bundle bundle=new Bundle();
        bundle.putParcelableArrayList("S",mDataServiceList);
        intent.putExtra("SE",bundle);
        startActivity(intent);
    }

    /**
     * create Test Daten
     */
    public void createTmpDealerAndServiceData(){
        Date date=new Date();
        mDbHelper.createDealer(mDatabase,"Autohaus Stumpf","Am Glockenturm","4",63814,"Mainaschaff","+49 6021 79080","kontakt@toyota-stumpf.de");
        Cursor mCursor=mDbHelper.searchSqlDealerElement(mDatabase,"Autohaus Stumpf");
        int dealerID=mCursor.getInt(mCursor.getColumnIndex("ID_DEALERSHIP"));
        for(int x=0; x<10; x++) {
            mDbHelper.createService(this.mDatabase, user.getId(), dealerID, "Reperatur", "Zündkerze gewechselt", 100, date);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        saveDataInSharedPreferences();
    }

    public void saveDataInSharedPreferences(){
        SharedPreferences sharedPreferences=getSharedPreferences("shared preferences",MODE_PRIVATE);
        SharedPreferences.Editor editor =sharedPreferences.edit();
        Gson gson=new Gson();
        String json=gson.toJson(user);
        editor.putString("LastUser",json);
        editor.apply();
    }

    public void loadDatafromSharedPreferences(){
        SharedPreferences sharedPreferences =getSharedPreferences("shared preferences",MODE_PRIVATE);
        Gson gson=new Gson();
        String json=sharedPreferences.getString("LastUser",null);
        Type type=new TypeToken<CurrentUser>(){}.getType();
        this.user=gson.fromJson(json,type);
        if(user==null){
            //user=new CurrentUser(100,"Test","Test","Test","Test");
            //Andreas HArdcodet in shared preference noch kein zusammenhangnimplementiert
        }
    }
}


package com.example.car_app.ui.dashboard;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.car_app.R;
import com.example.car_app.UserInterface;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
public class DashboardFragment extends Fragment {
    private ArrayList<CardItem> mDataServiceList;
    private RecyclerView mRecyclerView;
    private RecyclerViewCardAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private FloatingActionButton FloatButtonAddService;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        mDataServiceList=getActivity().getIntent().getBundleExtra("SE").getParcelableArrayList("S");

        if(mDataServiceList.size()!=0){

            buildRecyclerView(root);
        }
        setOnClickListener(root);
        return root;
    }


    /**
     * Create the recyclerview
     * @param root this view
     */
    public void buildRecyclerView(View root){
        mRecyclerView =root.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager=new LinearLayoutManager(getContext());
        mAdapter=new RecyclerViewCardAdapter(mDataServiceList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new RecyclerViewCardAdapter.OnItemClickListener() {
            @Override
            public void oItemClick(int position) {
                //Hier kommt das anZeigen eines neuen Fragments mit der kompletten werkstatt rechnung rein
                String text=mDataServiceList.get(position).getmLine1();
                Toast toast=Toast.makeText(getContext(),text,Toast.LENGTH_SHORT);
                toast.show();
                mAdapter.notifyItemChanged(position);
            }
        });
    }

    /**
     * On click listener des Float Action buttons
     * @param root
     */
    public void setOnClickListener(View root){
        this.FloatButtonAddService=root.findViewById(R.id.addCardButton);
        FloatButtonAddService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Hier kommt das erstellen eines neuen Service rein
                InsertNewServiceData();

            }
        });
    }

    /**
     * Erstellt ein neues element in mDataset
     */
    public void InsertNewServiceData(){
        mDataServiceList.add(0,new CardItem(R.drawable.ic_dashboard_black_24dp,"Newues Item","Autohaus Stumpf","25.07.1997",10));

        mAdapter.notifyItemInserted(0);
    }
}

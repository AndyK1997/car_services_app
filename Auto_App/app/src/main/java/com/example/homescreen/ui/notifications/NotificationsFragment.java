package com.example.homescreen.ui.notifications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.homescreen.R;

import org.w3c.dom.Text;

public class NotificationsFragment extends Fragment implements View.OnClickListener{

    private NotificationsViewModel notificationsViewModel;
    private TextView InsuranceName;
    private TextView InsuranceID;
    private TextView InsurancePhone;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        Button carInsurance =(Button) root.findViewById(R.id.Button_AutoVers);
        Button healthInsurance =(Button) root.findViewById(R.id.Button_Dak);
        Button towingService =(Button) root.findViewById(R.id.Button_Panne);
        InsuranceName=(TextView) root.findViewById(R.id.view_VersAgentur);
        InsuranceID=(TextView) root.findViewById(R.id.view_VersID);
        InsurancePhone=(TextView) root.findViewById(R.id.view_Tel);
        carInsurance.setOnClickListener(this);
        healthInsurance.setOnClickListener(this);
        towingService.setOnClickListener(this);
        return root;
    }


    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.Button_AutoVers:{
                String Name="V+K Versicherung"; String ID="565/589269-X"; String Phone="08002 153709";
                InsuranceName.setText("Name: "+Name);
                InsuranceID.setText("Knr: "+ID);
                InsurancePhone.setText("Tel:"+Phone);
                break;
            }
            case R.id.Button_Dak :{
                String Name="DAK-Gesundheit"; String ID="W284694328"; String Phone= "040 325 325 555";
                InsuranceName.setText("Name: "+Name);
                InsuranceID.setText("ID: "+ID);
                InsurancePhone.setText("Tel: "+Phone);
                break;
            }
            case R.id.Button_Panne:{
                String Name="DAK-Gesundheit"; String ID="565/589269-X"; String Phone= "08002 153709";
                InsuranceName.setText("Pannendienst");
                InsuranceID.setText("ID: "+ID);
                InsurancePhone.setText("Tel: "+Phone);
            }
        }
    }
}
